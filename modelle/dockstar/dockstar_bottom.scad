difference() {

	
	union() { 
		// Aussenkörper
		cube([86,86,25]);

		// Verbindung
		translate ( [ 0, 0, 25 ] ) { cube( [3, 30, 5]);}
		rotate(270) { translate ( [ -3 , 0, 25 ] ) { cube( [3, 25, 5]);}}

		translate ( [ 83, 0, 25 ] ) { cube( [3, 30, 5]);}
		rotate(270) { translate ( [ -3 , 60, 25 ] ) { cube( [3, 25, 5]);}}

		translate ( [ 0, 61, 25 ] ) { cube( [3, 25, 5]);}

		
	}

	// 8x8cm ausschneiden für die Platine
	translate ([ 3,3,10 ]) {cube([80,80,20]); } 

	// Boden
	// Der Boden braucht nicht komplett augedruckt werden, spart Material
	translate ([ 10, 10, 0] ) {cube( [ 66 ,65, 5] ); }
	// Teile ausschneiden und Platinenpfosten stehen lassen
	translate ([ 3,  9, 5 ])  {cube([80,53,10]); } 
	// Raum zwischen den beiden vorderen Platinenpfosten entfernen
	translate ([ 9,  3, 5 ])  {cube([68,56,10]); } 
	// Raum zwischen den hinteren Platinenpfosten ausschneiden
 	translate ([ 9, 62, 5 ]) {cube([68, 6, 5] );}
	// Raum hinter den hinteren Pfosten ausschneiden
	translate ([ 3, 68, 5 ]) {cube([80, 15, 5] );}

	// Rückwand ausschneiden
	translate ( [ 9, 83, 10 ] ) { cube( [ 67, 3, 15 ] ) ; }

	// seitlichen USB-Port freimachen
	translate ( [ 83, 43, 10 ] ) { cube( [ 3, 18, 15 ] ) ; }
}


